// This mixins methods and properties will merge into other class that it called
// like a class extends class
export default {
  methods: {
    get () {
      fetch('/api/get')
    }
  },
  data () {
    return {
      text: null
    }
  },
  computed: {

  }
}
